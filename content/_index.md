---
date: 2020-03-17
tags: ["contributing", "about"]
---

This is an initiative to facilitate learning from home for our community.

Additions are most welcome. If there's a skill you'd like to share with others consider contributing.

Current we have content for :
- [Rust](/post/rust/)
- [C](/post/c/)
- [Python](/post/python/)
- [Django](/post/django/)
- [Ruby](/post/ruby/)
