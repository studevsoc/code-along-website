---
title: "Contributors"
date: 2020-03-18T20:54:47+05:30
tags: ["contributors"]
---

This project is a combined effort of many people. We would like to recognise the contributors their contributions in this post.

## Website
- [Abraham Raji](https://gitlab.com/avronr)
- [Ajmal Aju](https://gitlab.com/Ajmalaju)
- [Syam Kumar](https://gitlab.com/syamkumar)

## Content
- [Dev Dutt](https://gitlab.com/Devdutt): Rust
- [Ajmal Aju](https://gitlab.com/Ajmalaju): Django
- [Kiran Nambiar](https://gitlab.com/itsmekirannambiar): Python
- [Abraham Raji](https://gitlab.com/avronr): C

## Promotional Material
- Bhairavi Shah
- Levi Vaguez